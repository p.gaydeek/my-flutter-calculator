import 'package:decimal/decimal.dart';
import 'package:flutter/material.dart';
import 'package:mycalculator/button_values.dart';

class MyCalculator extends StatefulWidget {
  const MyCalculator({super.key});

  @override
  State<MyCalculator> createState() => _MyCalculatorState();
}

class _MyCalculatorState extends State<MyCalculator> {
  String number1 = "";
  String operand = "";
  String number2 = "";
  String historyResult = "";
  bool calculateBtnClicked = false;

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            // history
            Expanded(
              child: SingleChildScrollView(
                reverse: true,
                child: Container(
                  alignment: Alignment.bottomRight,
                  padding: const EdgeInsets.all(16),
                  child: SelectableText(
                    historyResult,
                    style: const TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.bold,
                      color: Colors.grey,
                    ),
                    textAlign: TextAlign.end,
                  ),
                ),
              ),
            ),
            // output
            Expanded(
              child: SingleChildScrollView(
                reverse: true,
                child: Container(
                  alignment: Alignment.bottomRight,
                  padding: const EdgeInsets.all(16),
                  child: SelectableText(
                    "$number1$operand$number2".isEmpty
                        ? "0"
                        : "$number1$operand$number2",
                    style: const TextStyle(
                      fontSize: 48,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.end,
                  ),
                ),
              ),
            ),
            // buttons
            Wrap(
              children: Btn.buttonValues
                  .map(
                    (value) => SizedBox(
                      width: screenSize.width / 4,
                      height: screenSize.height / 7,
                      child: buildButton(value),
                    ),
                  )
                  .toList(),
            )
          ],
        ),
      ),
    );
  }

  Widget buildButton(value) {
    return Padding(
      padding: const EdgeInsets.all(3.0),
      child: Material(
        color: value == Btn.calculate ? Colors.green : (Colors.black87),
        clipBehavior: Clip.hardEdge,
        shape: OutlineInputBorder(
          borderSide: const BorderSide(
            color: Colors.white24,
          ),
          borderRadius: BorderRadius.circular(100),
        ),
        child: InkWell(
          onTap: () => onBtnClick(value),
          child: Center(
            child: Text(
              value,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 32,
                color: getBtnColor(value),
              ),
            ),
          ),
        ),
      ),
    );
  }

  // Making calculations on clicked button
  void onBtnClick(String value) {
    if (value == Btn.del) {
      delete();
      return;
    }

    if (value == Btn.clr) {
      clearAllOutput();
      return;
    }

    if (value == Btn.clrLast) {
      clearLast();
      return;
    }

    if (value == Btn.calculate) {
      calculateResult();
      return;
    }

    if (value == Btn.invert) {
      invertValue();
      return;
    }

    appendValue(value);
  }

  // Inverts the value
  void invertValue() {
    if (number2.isNotEmpty) {
      // check if number2 is negative and we invert to positive and removing brackets
      if (number2.contains("-")) {
        number2 = number2.substring(2, number2.length - 1);
      } else {
        // if number is positive and we invert to negative we add brackets
        number2 = "(-$number2)";
      }
    } else if (number1.isNotEmpty) {
      if (number1.contains("-")) {
        number1 = number1.substring(1, number1.length);
      } else {
        number1 = "-$number1";
      }
    }

    setState(() {});
  }

  // Calculates the resultcalculateBtnClicked
  void calculateResult() {
    if (number1.isEmpty) return;
    if (operand.isEmpty) return;
    if (number2.isEmpty) return;

    // adding values to history
    historyResult = "$number1 $operand $number2 = ";

    // check if number1 is negative and removes brackets
    if (number1.contains("(")) {
      number1 = number1.substring(1, number1.length - 1);
    }

    // check if number2 is negative and removes brackets
    if (number2.contains("(")) {
      number2 = number2.substring(1, number2.length - 1);
    }

    final Decimal num1 = Decimal.parse(number1);
    final Decimal num2 = Decimal.parse(number2);

    Decimal result = Decimal.zero;

    switch (operand) {
      case Btn.add:
        result = num1 + num2;
        break;
      case Btn.subtract:
        result = num1 - num2;
        break;
      case Btn.multiply:
        result = num1 * num2;
        break;
      case Btn.divide:
        if (num2 != Decimal.zero) {
          result = (num1 / num2).toDecimal(scaleOnInfinitePrecision: 10);
        } else {
          setState(() {
            historyResult = "Can't divide by 0";
          });
          return;
        }
        break;
      default:
    }
    setState(() {
      number1 = result.toString();

      // check if the result is whole number so we remove ".0"
      if (number1.endsWith(".0")) {
        number1 = number1.substring(0, number1.length - 2);
      }

      operand = "";
      number2 = "";
      historyResult += number1;
      calculateBtnClicked = true;
    });
  }

  // Clears all output
  void clearAllOutput() {
    setState(() {
      number1 = "";
      operand = "";
      number2 = "";
      historyResult = "";
    });
  }

  // Clears last output
  void clearLast() {
    if (number2.isNotEmpty) {
      number2 = "";
    } else {
      number1 = "";
      operand = "";
    }

    setState(() {});
  }

  // Delete last character
  void delete() {
    if (number2.isNotEmpty) {
      // check if number2 is negative so it wont become "(-23"
      // and if there is one number left to remove all ex: "(-3)" => ""
      if (number2.endsWith(")")) {
        number2 = number2.length >= 5
            ? "${number2.substring(0, number2.length - 2)})"
            : "";
      } else {
        number2 = number2.substring(0, number2.length - 1);
      }
    } else if (operand.isNotEmpty) {
      operand = "";
    } else if (number1.isNotEmpty) {
      // check if number1 is negative and if there is one number left to remove all ex: "-3" => ""
      if (number1.startsWith("-")) {
        number1 =
            number1.length > 2 ? number1.substring(0, number1.length - 1) : "";
      } else {
        number1 = number1.substring(0, number1.length - 1);
      }
    }

    setState(() {});
  }

  // Appends value to the end
  void appendValue(String value) {
    // if is operand and not "."
    if (value != Btn.dot && int.tryParse(value) == null) {
      // operand pressed
      if (operand.isNotEmpty && number2.isNotEmpty) {
        calculateResult();
      }
      if (number1.isNotEmpty) {
        operand = value;
      }
      calculateBtnClicked = false;
    }
    // assign value to number1 variable
    else if (number1.isEmpty || operand.isEmpty) {
      // check if value is "." | ex. number1 = "1.2"
      if (value == Btn.dot && number1.contains(Btn.dot)) return;
      if (value == Btn.dot && number1.isEmpty) {
        // ex. number1 = "" | "0"
        value = "0.";
      }

      // Sets limit to 15 digits
      if (!calculateBtnClicked) {
        // check is "-0" is in output and if other number is pressed to replace the "0" with the value
        if (number1.startsWith("-0")) {
          number1 = "-$value";
        } else if (number1.length < 15) {
          number1 += value;
        }
      } else {
        number1 = value;
        calculateBtnClicked = false;
      }

      // check if the first digit is not "0" so it wont become "0123"
      if (number1.startsWith("0") &&
          number1.length > 1 &&
          !number1.contains(".")) {
        number1 = number1.substring(1);
      }
    }
    // assign value to number2 variable
    else if (number2.isEmpty || operand.isNotEmpty) {
      // check if value is "." | ex. number2 = "1.2"
      if (value == Btn.dot && number2.contains(Btn.dot)) return;
      if (value == Btn.dot && number2.isEmpty) {
        value = "0.";
      }

      // check if number is negative, so it wont become "(-23)23" if add more numbers
      // and sets limit to 15 digits without "(-)"
      if (number2.contains("(") && number2.length < 18) {
        number2 = "${number2.substring(0, number2.length - 1)}$value)";
      } else if (!calculateBtnClicked) {
        if (number2.length < 15) {
          number2 += value;
        }
      } else if (calculateBtnClicked) {
        number2 = value;
        calculateBtnClicked = false;
      }

      // check if the first digit is not "0" so it wont become "0123"
      if (number2.startsWith("0") &&
          number2.length > 1 &&
          !number2.contains(".")) {
        number2 = number2.substring(1);
      }

      // check if number2 starts with negative "-0" and if other number value
      // is pressed so it will replace the value
      if (number2.startsWith("(-0") &&
          number2.length > 3 &&
          !number2.contains(".")) {
        number2 = "(-$value)";
      }
    }
    setState(() {});
  }

  Color getBtnColor(value) {
    return [Btn.clrLast, Btn.del, Btn.clr].contains(value)
        ? Colors.red
        : [Btn.multiply, Btn.add, Btn.subtract, Btn.divide].contains(value)
            ? Colors.green
            : Colors.white;
  }
}
