import 'package:flutter/material.dart';
import 'package:mycalculator/mycalculator.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PGCalculator',
      theme: ThemeData.dark(),
      home: const MyCalculator(),
    );
  }
}
